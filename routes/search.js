const express = require("express");
const router = express.Router();
const { Customer } = require("../models/customer");
let glo;
router.get("/", async (req, res) => {
  let name = req.query.search;
  let f = req.query.filter;

  const customer = await Customer.find({
    name: new RegExp(".*" + name + ".*", "i"),
  });
  glo = customer;
  if (!customer) return res.status(404).send("The Customer was not found.");
  res.render("index", { customers: customer });
});

router.get("/sort", async (req, res) => {
  let age_so = req.query;

  const customer = await Customer.find().sort("age");

  if (!customer) return res.status(404).send("The Customer was not found.");
  res.render("index", { customers: customer });
});

router.get("/sort", (req, res) => {
  res.send("GET Request Called");
});
module.exports = router;
