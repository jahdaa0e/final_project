const express = require("express");
const router = express.Router();
const { validate, Customer } = require("../models/customer");
let flag = true;
router.get("/index", async (req, res) => {
  const customers = await Customer.find().sort({ name: 1 });
  res.render("index", { customers: customers });
});

router.get("/",async(req,res) =>{
  const total_customers = await Customer.find().count();
  res.render("dashboard",{total_customers:total_customers});
});

router.get("/sortbyage", async (req, res) => {
  const customers = await Customer.find().sort({
    age: 1,
  });
  res.render("index", { customers: customers });
});
router.get("/customer", async (req, res) => {
  const customers = await Customer.find();
  res.render("index", { customers: customers });
});

router.get("/sortbygold", async (req, res) => {
  const customers = await Customer.find().sort({ isGold: 1 });
  res.render("index", { customers: customers });
});

router.get("/sortbydate", async (req, res) => {
  const customers = await Customer.find().sort({ regestration: 1 });
  res.render("index", { customers: customers });
});

router.get("/sortbyname", async (req, res) => {
  const customers = await Customer.find().sort({ name: 1 });
  res.render("index", { customers: customers });
});


router.get("/counts", async (req, res) => {
  const customers = await Customer.find().exec(function(err, res) {
    var count = res.length
    console.log(count)
  });
  res.render("index", { customers: customers });
});

router.get("/add", (req, res) => {
  res.render("add-customer");
});
router.get("/show/:id", async (req, res) => {
  const customer = await Customer.findById(req.params.id);
  if (!customer)
    return res
      .status(404)
      .send("The Customer with the given ID was not found.");
  res.render("show-customer", { customer: customer });
});

router.get("/delete/:id", async (req, res) => {
  let customer = await Customer.findByIdAndRemove(req.params.id);
  if (!customer)
    return res
      .status(404)
      .send("The Customer with the given ID was not found.");
  res.redirect("/api/customer");
});
router.put("/customers/:id", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  console.log(req.body);
  let customer = await Customer.findByIdAndUpdate(req.params.id, {
    name: req.body.name,
    age: req.body.age,
    // regestration: req.body.date,
    mobile: req.body.mobile,
    isGold: req.body.isGold,
  });
  if (!customer)
    return res
      .status(404)
      .send("The Customer with the given ID was not found.");
  res.redirect("/api/customer");
});

router.post("/", async (req, res) => {
  const vr = validate(req.body);
  console.log("v  " + JSON.stringify(vr));
  if (vr.error) return res.status(400).send(error.details[0].message);

  let customer = new Customer({
    name: req.body.name,
    mobile: req.body.mobile,
    isGold: req.body.isGold,
  });
  try {
    customer = await customer.save();
  } catch (err) {
    console.log(err);
  }
  res.redirect("/api/customer");
});

module.exports = router;
