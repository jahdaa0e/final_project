FROM node:latest

ENV NOE_ENV=production
ENV PORT=3000
WORKDIR /var/www
COPY . /var/www
RUN npm install
RUN ls -al
EXPOSE $PORT
# ENTRYPOINT [ "npm","start" ]

#ENTRYPOINT [ "sleep","60001"]
CMD npm start
# CMD ["npm", "start"]
