const Joi = require("joi");
const mongoose = require("mongoose");
const myCustomJoi = Joi.extend(require("joi-phone-number"));

const Customer = mongoose.model(
  "Customer",
  new mongoose.Schema({
    name: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 255,
    },
    age: {
      type: String,
      required: true,
      default: 24,
    },
    // regestration: {
    //   type: String,
    //   required: true,
    //   default: new Date(),
    // },
    mobile: {
      type: String,
      required: true,
      minlength: 10,
      maxlength: 25,
    },
    isGold: {
      type: Boolean,
      default: false,
    },
  })
);

// but the same condition above for the joi
function validateCustomer(customer) {
  const schema = Joi.object({
    name: Joi.string().min(3).max(255).required(),
    age: Joi.required(),
    // regestration: Joi.required(),
    mobile: myCustomJoi.string().phoneNumber().min(10).max(25).required(),
    isGold: Joi.boolean(),
  });
  console.log(customer.mobile.length);
  let v = schema.validate(customer);
  console.log("validattion: " + JSON.stringify(v));
  return v;
}

exports.Customer = Customer;
exports.validate = validateCustomer;
